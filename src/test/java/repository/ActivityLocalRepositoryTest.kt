package repository

import domain.Activity
import domain.Contact
import org.hamcrest.CoreMatchers.*
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.both
import org.junit.Before
import org.junit.Test
import java.sql.Time
import java.time.Duration

class ActivityLocalRepositoryTest {

    private lateinit var repositoryActivity: Repository<Activity>
    private lateinit var repositoryContact: Repository<Contact>

    @Before
    fun setUp() {
        repositoryActivity = LocalRepository({
            id, element -> element.copy(id = id.incrementAndGet())})
        repositoryContact = LocalRepository({
            id, element -> element.copy(id = id.incrementAndGet())})
    }

    @Test
    fun whenSavingContact_WithValidContact_ExpectContactSaved() {
        // given:
        val contact= Contact("name", "address", "000", "test@test.com")
                .let {repositoryContact.save(it)}
        val subject = Activity(title="Title",
                description = "test", location = "Location",
                startingHour = Time.valueOf("00:00:01"), duration = Duration.ofDays(1))
                .copy(contacts = listOf(contact))
        // when:
        val result = repositoryActivity.save(subject)
        // then:
        assertThat(repositoryActivity.all().first(), both(`is`(equalTo(result)))
                .and(`is`(equalTo(subject.copy(id = 1)))))
    }
}