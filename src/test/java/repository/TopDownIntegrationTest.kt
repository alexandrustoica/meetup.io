package repository

import com.nhaarman.mockito_kotlin.mock
import domain.Activity
import domain.Contact
import domain.User
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.*
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import ui.ConsoleManager
import java.io.*
import java.sql.Time
import java.time.Duration
import java.util.*
import java.text.SimpleDateFormat


class TopDownIntegrationTest {

    private lateinit var repositoryContact: Repository<Contact>
    private lateinit var repositoryActivity: Repository<Activity>

    private val outContent = ByteArrayOutputStream()

    private val mockUserRepository: Repository<User> = mock()
    private val mockContactRepository: Repository<Contact> = mock()
    private val mockActivityRepository: Repository<Activity> = mock()

    private val consoleManager = ConsoleManager(
            mockUserRepository,
            mockContactRepository,
            mockActivityRepository
    )

    @Suppress("UNCHECKED_CAST")
    private fun <T> any(): T = Mockito.any<T>().let { null as T }

    @Before
    fun setUp() {
        System.setOut(PrintStream(outContent))
        repositoryContact =
                LocalRepository({ id, element -> element.copy(id = id.incrementAndGet()) })
        repositoryActivity =
                LocalRepository({ id, element -> element.copy(id = id.incrementAndGet()) })
    }


    @Test
    fun whenSavingContact_WithValidContact_ExpectContactSaved() {
        // given:
        val subject = Contact("name", "address", "000", "test@test.com")
        // when:
        val result = repositoryContact.save(subject)
        // then:
        assertThat(repositoryContact.all().first(), both(`is`(equalTo(result)))
                .and(`is`(equalTo(subject.copy(id = 1)))))
    }

    @Test
    fun whenCreatingActivity_WithValidData_ExpectActivityCreated() {
        // given:
        val contact = Contact("name", "address", "000", "test@test.com").copy(id = 1)
        val activity = Activity(title = "Title",
                description = "test", location = "Location",
                startingHour = Time.valueOf("00:00:01"), duration = Duration.ofDays(1))
                .copy(id = 1, contacts = listOf(contact))
        val specialInputConsole = ConsoleManager(
                mockUserRepository,
                mockContactRepository,
                mockActivityRepository
        )
        // when:
        //`when`(mockContactRepository.findById(any())).thenReturn(contact)
        System.setIn(ByteArrayInputStream(File("input.txt").readBytes()))
        `when`(mockActivityRepository.save(any())).thenReturn(activity)
        // consoleManager.readAndCreateActivity()
        // then:
        print(outContent.toString())
    }

    @Test
    fun whenTestingConsoleOutput_WithMockDependencies_ExpectCorrectResult() {
        // given:
        val contact = Contact("name", "address", "000", "test@test.com").copy(id = 1)
        val activity = Activity(title = "Title",
                description = "test", location = "Location",
                startingHour = Time.valueOf("00:00:01"), duration = Duration.ofDays(1))
                .copy(id = 1, contacts = listOf(contact))
        val date = Calendar.getInstance()
                .also { it.add(Calendar.DAY_OF_MONTH, -4) }
        // when:
        System.setIn(ByteArrayInputStream(
                SimpleDateFormat("dd MM yyyy").format(date.time).toByteArray()))
        `when`(mockActivityRepository.findBy(any())).thenReturn(listOf(activity))
        consoleManager.showReport()
        // then:
        assertThat(outContent.toString(), containsString(activity.toString()))
    }
}