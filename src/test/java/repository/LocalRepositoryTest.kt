package repository

import domain.Contact
import org.hamcrest.CoreMatchers.*
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.both
import org.junit.Before
import org.junit.Test

class LocalRepositoryTest {

    private lateinit var repository: Repository<Contact>

    @Before
    fun setUp() {
        repository = LocalRepository({
            id, element -> element.copy(id = id.incrementAndGet())})
    }

    @Test
    fun whenSavingContact_WithValidContact_ExpectContactSaved() {
        // given:
        val subject = Contact("name", "address", "000", "test@test.com")
        // when:
        val result = repository.save(subject)
        // then:
        assertThat(repository.all().first(), both(`is`(equalTo(result)))
                .and(`is`(equalTo(subject.copy(id = 1)))))
    }
}