package repository

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import domain.Activity
import domain.Contact
import org.hamcrest.CoreMatchers.*
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Test
import java.io.File
import java.sql.Time
import java.time.Duration

class ActivityJsonRepositoryTest {

    private lateinit var repositoryActivity: Repository<Activity>
    private val activityJsonFile = "activity_test.json"
    private val typeForListOfActivities = object : TypeToken<List<Activity>>() {}.type


    private lateinit var repositoryContact: Repository<Contact>
    private val contactJsonFile = "test.json"
    private val typeForListOfContacts = object : TypeToken<List<Contact>>() {}.type


    @Before
    fun setUp() {
        File(contactJsonFile).writeText("[]")
        File(activityJsonFile).writeText("[]")
        repositoryContact = JsonRepository(contactJsonFile, typeForListOfContacts,
                LocalRepository({ id, element -> element.copy(id = id.incrementAndGet()) }))
        repositoryActivity = JsonRepository(activityJsonFile, typeForListOfActivities,
                LocalRepository({ id, element -> element.copy(id = id.incrementAndGet()) }))
    }

    @Test
    fun whenSavingContact_WithValidContact_ExpectContactSavedInFile() {
        // given:
        val contact = Contact("name", "address", "000", "test@test.com")
                .let { repositoryContact.save(it) }
        val activity = Activity(title="Title",
                description = "test", location = "Location",
                startingHour = Time.valueOf("00:00:01"), duration = Duration.ofDays(1))
                    .copy(contacts = listOf(contact))
        val subject = "[${Gson().toJson(activity.copy(id = 1))}]"
        // when:
        val result = repositoryActivity.save(activity)
                .let{ File(activityJsonFile).readLines().first() }
        // then:
        assertThat(result, `is`(equalTo(subject)))
    }
}