package repository

import domain.Activity
import org.hamcrest.CoreMatchers.`is`
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Test
import java.util.*

class LocalRepositoryWhiteBoxTest {

    private lateinit var repository: Repository<Activity>

    @Before
    fun setUp() {
        repository = LocalRepository({ id, element ->
            element.copy(id = id.incrementAndGet()) })
    }

    @Test
    fun whenSelectingAllActivity_BasedOnDate_ExpectCorrectActivities() {
        // given:
        listOf<Activity>().map{ repository.save(it) }
        // when:
        val result = repository.selectBy { it.created > Calendar.getInstance() }
        // then:
        assertThat(result, `is`(emptyList()))
    }

    @Test
    fun whenSelectingActivities_DateAfterToday_ExpectOneActivity() {
        // given:
        val activities = listOf(
                Activity(created=Calendar.getInstance()),
                Activity(created=Calendar.getInstance()
                        .also { it.add(Calendar.DAY_OF_MONTH, -1) }),
                Activity(created=Calendar.getInstance()
                        .also { it.add(Calendar.DAY_OF_MONTH, 1) }))
                .map{repository.save(it)}
        val subject = activities.subList(2, 3)
        // when:
        val result = repository.selectBy { it.created > Calendar.getInstance() }
        // then:
        assertThat(result, `is`(subject))
    }

    @Test
    fun whenSelectingActivities_DateAfterToday_ExpectZeroActivities() {
        // given:
        listOf(Activity(created=Calendar.getInstance()),
                Activity(created=Calendar.getInstance()
                        .also { it.add(Calendar.DAY_OF_MONTH, -1) }))
                .map{repository.save(it)}
        // when:
        val result = repository.selectBy { it.created > Calendar.getInstance() }
        // then:
        assertThat(result, `is`(emptyList()))
    }
}