package repository

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import domain.Contact
import org.hamcrest.CoreMatchers.*
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Test
import java.io.File

class JsonRepositoryTest {

    private lateinit var repository: Repository<Contact>
    private val jsonFileName = "test.json"
    private val typeForListOfContacts = object : TypeToken<List<Contact>>() {}.type

    @Before
    fun setUp() {
        File(jsonFileName).writeText("[]")
        repository = JsonRepository(jsonFileName, typeForListOfContacts,
                LocalRepository({ id, element -> element.copy(id = id.incrementAndGet()) }))
    }

    @Test
    fun whenSavingContact_WithValidContact_ExpectContactSavedInFile() {
        // given:
        val contract = Contact("name", "address", "000", "test@test.com")
        val subject = "[${Gson().toJson(contract.copy(id = 1))}]"
        // when:
        val result = repository.save(contract)
                .let { File(jsonFileName).readLines().first() }
        // then:
        assertThat(result, `is`(equalTo(subject)))
    }
}