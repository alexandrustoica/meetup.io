package validator

import exception.InvalidData
import exception.InvalidPhoneNumber
import org.hamcrest.CoreMatchers.*
import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.Parameterized

@RunWith(value = Parameterized::class)
class PhoneNumberValidatorTest(
        private val subject: String,
        private val result: String,
        private val isExceptionExpected: Boolean) {

    companion object {
        @JvmStatic
        @Parameterized.Parameters(
                name = "{index}: Checking PhoneNumber {0} = {1}")
        fun data(): Collection<Array<Any>> = listOf(
                arrayOf("2345352525", "2345352525", false),
                arrayOf("", "", true),
                arrayOf("a", "a", true),
                arrayOf("000000000", "000000000", true),
                arrayOf("00000000000", "00000000000", true)
        )
    }

    @Test
    fun whenCheckingPhoneNumber_ExpectExpectedValue() {
        if (isExceptionExpected)
            whenCheckingPhoneNumber_WithInvalidPhoneNumber_ExpectException()
        else whenCheckingPhoneNumber_WithValidPhoneNumber_ExpectPhoneNumberReturned()
    }

    private fun
            whenCheckingPhoneNumber_WithValidPhoneNumber_ExpectPhoneNumberReturned() =
            assertThat(PhoneNumberValidator().validate(subject),
                    `is`(equalTo(result)))

    private fun
            whenCheckingPhoneNumber_WithInvalidPhoneNumber_ExpectException() =
            try { PhoneNumberValidator().validate(subject) }
            catch (error: InvalidData) {
                assertThat(error, instanceOf(InvalidPhoneNumber::class.java)) }
}