package validator

import exception.InvalidData
import exception.InvalidEmail
import org.hamcrest.CoreMatchers.*
import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.Parameterized
import java.util.*
import kotlin.streams.asSequence


@RunWith(value = Parameterized::class)
class EmailValidatorTest(
        private val subject: String,
        private val result: String,
        private val isExceptionExpected: Boolean) {

    private class GeneratedEmail(private val size: Long) {

        fun generate(long: Long): String =
                Random().ints(long).asSequence()
                        .map { it.toString() }.joinToString("")

        fun value(): String =
            "${generate(size / 2 - 5)}@${generate(size / 2 - 5)}.${generate(10)}"
    }

    companion object {
        @JvmStatic
        @Parameterized.Parameters(name = "{index}: Checking Email {0} = {1}")
        fun data(): Collection<Array<Any>> {
            val emailWith254Chars = GeneratedEmail(254).value()
            val emailWith255Chars = GeneratedEmail(255).value()
            val emailWith256Chars = GeneratedEmail(256).value()
            return listOf(
                    arrayOf("test@test.com", "test@test.com", false),
                    arrayOf("@a.a", "@a.a", true),
                    arrayOf("", "", true),
                    arrayOf("a", "a", true),
                    arrayOf("a@a.a", "a@a.a", false),
                    arrayOf("aa@a.a", "aa@a.a", false),
                    arrayOf(emailWith254Chars, emailWith254Chars, false),
                    arrayOf(emailWith255Chars, emailWith255Chars, false),
                    arrayOf(emailWith256Chars, emailWith256Chars, false)
            )
        }
    }


    @Test
    fun whenCheckingEmail_ExpectExpectedResult() {
        if (isExceptionExpected) whenCheckingEmail_WithInvalidEmail_ExpectException()
        else whenCheckingEmail_withValidEmail_expectValidEmailReturned()
    }

    private fun whenCheckingEmail_withValidEmail_expectValidEmailReturned() =
            assertThat(EmailValidator().validate(subject), `is`(equalTo(result)))

    private fun whenCheckingEmail_WithInvalidEmail_ExpectException() =
            try { EmailValidator().validate(subject) }
            catch (error: InvalidData) {
                assertThat(error, instanceOf(InvalidEmail::class.java)) }
}