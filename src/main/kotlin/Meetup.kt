import com.google.gson.reflect.TypeToken
import domain.Activity
import domain.Contact
import domain.User
import repository.JsonRepository
import repository.LocalRepository
import repository.Repository
import ui.ConsoleAuthentication

fun main(args: Array<String>) {

    val typeForListOfUsers = object : TypeToken<List<User>>() {}.type
    val typeForListOfContacts = object : TypeToken<List<Contact>>() {}.type
    val typeForListOfActivities = object : TypeToken<List<Activity>>() {}.type

    val fileContactRepository: Repository<Contact> = JsonRepository(
            "contacts.json", typeForListOfContacts,
            LocalRepository({ id, element -> element.copy(id = id.incrementAndGet()) }))

    val fileActivityRepository: Repository<Activity> = JsonRepository(
            "activities.json", typeForListOfActivities,
            LocalRepository({ id, element -> element.copy(id = id.incrementAndGet()) }))

    val fileUserRepository: Repository<User> = JsonRepository(
            "users.json", typeForListOfUsers,
            LocalRepository({ id, element -> element.copy(id = id.incrementAndGet()) }))

    ConsoleAuthentication(
            fileUserRepository,
            fileContactRepository,
            fileActivityRepository).run()
}