package exception

open class InvalidData(override val message: String) : RuntimeException()