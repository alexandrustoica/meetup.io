package repository

import domain.Entity
import java.util.concurrent.atomic.AtomicInteger

class LocalRepository<T : Entity>(
        private val data: MutableList<T>,
        private val identity: AtomicInteger,
        private val identityGenerator: (AtomicInteger, T) -> T) : Repository<T> {

    constructor(identityGenerator: (AtomicInteger, T) -> T) :
            this(mutableListOf(), AtomicInteger(0), identityGenerator)

    override fun save(element: T): T =
            identityGenerator.invoke(identity, element)
                    .also { data.add(it) }

    override fun findById(id: Int): T? =
            data.firstOrNull { it.id == id }

    override fun findBy(criteria: (T) -> Boolean): List<T> =
            data.filter { criteria(it) }

    override fun selectBy(criteria: (T) -> Boolean): List<T> {
        // this is stupid and created just for white-box testing
        var result = listOf<T>()
        for (element in data)
            if (criteria(element))
                result += listOf(element)
        return result
    }

    override fun getBy(criteria: (T) -> Boolean): T? =
            data.findLast(criteria)

    override fun all(): List<T> = data
}
