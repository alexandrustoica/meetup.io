package repository

import domain.Entity

interface Repository<T : Entity> {
    fun save(element: T): T
    fun all(): List<T>
    fun findById(id: Int): T?
    fun findBy(criteria: (T) -> Boolean): List<T>
    fun selectBy(criteria: (T) -> Boolean): List<T>
    fun getBy(criteria: (T) -> Boolean): T?
}