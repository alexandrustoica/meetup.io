package repository

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.google.gson.stream.JsonReader
import domain.Entity
import java.io.FileReader
import java.io.FileWriter
import java.lang.reflect.Type


class JsonRepository<T : Entity>(
        private val fileName: String,
        private val type: Type,
        override val repository: Repository<T>) :
        RepositoryDecorator<T>(repository) {

    override fun save(element: T): T =
            repository.save(element).also { updateFileWithNewElements() }

    private fun updateFileWithNewElements() = FileWriter(fileName)
            .also { it.write(Gson().toJson(repository.all(), type)) }
            .also { it.close() }

    override fun all(): List<T> =
            Gson().fromJson(JsonReader(FileReader(fileName)), type)
}