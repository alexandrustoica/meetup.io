package repository

import domain.Entity

open class RepositoryDecorator<T : Entity>(
        protected open val repository: Repository<T>) : Repository<T> {

    override fun selectBy(criteria: (T) -> Boolean): List<T> =
            repository.selectBy(criteria)

    override fun save(element: T): T = repository.save(element)
    override fun findById(id: Int): T? = repository.findById(id)

    override fun findBy(criteria: (T) -> Boolean): List<T> =
            all().filter(criteria)

    override fun getBy(criteria: (T) -> Boolean): T? =
            all().findLast(criteria)

    override fun all(): List<T> = repository.all()
}