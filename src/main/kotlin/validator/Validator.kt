package validator

interface Validator<T> {
    fun validate(element: T): T
}