package validator

import exception.InvalidPhoneNumber

class PhoneNumberValidator: ValidatorStrategy<String> {
    override fun validate(element: String): String =
            if(element.matches(Regex("\\d{10}"))) element
            else throw InvalidPhoneNumber()
}