package validator

interface ValidatorStrategy<T> : Validator<T>