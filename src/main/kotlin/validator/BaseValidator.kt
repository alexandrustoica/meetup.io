package validator

class BaseValidator<T>(
        private val strategy: ValidatorStrategy<T>) : Validator<T> {

    override fun validate(element: T): T = strategy.validate(element)
}