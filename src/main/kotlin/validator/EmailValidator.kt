package validator

import exception.InvalidEmail

class EmailValidator : ValidatorStrategy<String> {
    override fun validate(element: String): String =
            if (element.matches(Regex(".+@.+\\..+"))) element
            else throw InvalidEmail()
}