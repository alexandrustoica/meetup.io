package domain

interface Entity {
    val id: Int
}