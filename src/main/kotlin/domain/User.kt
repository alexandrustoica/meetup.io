package domain

data class User(override val id: Int,
                val username: String,
                val password: String,
                val name: String) : Entity {

    constructor(username: String, password: String, name: String) :
            this(0, username, password, name)

    override fun toString(): String = "$id $username $password $name"
}