package domain

import repository.Repository
import java.util.*

data class Report(
        private val repository: Repository<Activity>,
        private val date: Date) {

    fun activities(): List<Activity> = repository
            .findBy { it.created.timeInMillis > date.time }
            .sortedBy { it.startingHour }

    override fun toString(): String = activities()
            .map { it.toString() }
            .reduce { accumulator, activity -> "$accumulator \n $activity" }
}