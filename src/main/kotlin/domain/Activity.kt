package domain

import java.sql.Time
import java.time.Duration
import java.time.LocalTime
import java.util.*


data class Activity(override val id: Int,
                    private val title: String,
                    private val description: String,
                    val created: Calendar,
                    private val location: String,
                    val startingHour: Time,
                    private val duration: Duration,
                    private val contacts: List<Contact>) : Entity {

    constructor(title: String, description: String, location: String,
                startingHour: Time, duration: Duration) :
            this(0, title, description, Calendar.getInstance(),
                    location, startingHour, duration, listOf())

    constructor(title: String, description: String, location: String,
                startingHour: Time, duration: Duration, contacts: List<Contact>) :
            this(0, title, description, Calendar.getInstance(),
                    location, startingHour, duration, contacts)

    constructor(title: String, description: String, location: String,
                startingHour: Time, duration: Duration, created: Calendar,
                contacts: List<Contact>) :
            this(0, title, description, created,
                    location, startingHour, duration, contacts)

    constructor(created: Calendar) :
            this("default", "default", "default",
                    Time.valueOf(LocalTime.now()),
                    Duration.ofDays(10), created, listOf())

    fun addContactToActivity(contact: Contact) =
            this.copy(contacts = contacts + contact)

    fun deleteContactFromActivtiy(contact: Contact) =
            this.copy(contacts = contacts.filter { it == contact })
}