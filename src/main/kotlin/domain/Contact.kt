package domain

data class Contact(override val id: Int,
                   private val name: String,
                   private val address: String,
                   private val phoneNumber: String,
                   private val email: String) : Entity {

    constructor(name: String, address: String, phoneNumber: String, email: String) :
            this(0, name, address, phoneNumber, email)

    constructor() :
            this(0, "default", "default", "default", "default")

    fun replaceId(id: Int): Contact = copy(id = id)

    override fun toString(): String = "$id $name $address $phoneNumber $email"
}