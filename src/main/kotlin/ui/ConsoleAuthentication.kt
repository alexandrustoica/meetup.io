package ui

import domain.Activity
import domain.Contact
import domain.User
import repository.Repository
import java.util.*

class ConsoleAuthentication(
        private val userRepository: Repository<User>,
        private val contactRepository: Repository<Contact>,
        private val activityRepository: Repository<Activity>) : Console() {

    enum class Option : DisplayOption {
        LOGIN, EXIT
    }

    override val getUserOptionFrom = fun(index: Int): DisplayOption =
            Option.values()[index - 1]

    override val optionMessage: HashMap<DisplayOption, String> = hashMapOf(
            Option.LOGIN to "1. Login",
            Option.EXIT to "2. Exit"
    )

    override val action: HashMap<DisplayOption, () -> Unit> = hashMapOf(
            Option.LOGIN to { loginUser().also { run() } },
            Option.EXIT to { }
    )

    private fun loginUser() {
        val username = println("Username: ").let { Scanner(System.`in`).nextLine() }
        val password = println("Password: ").let { Scanner(System.`in`).nextLine() }
        userRepository.getBy { it.username == username && it.password == password }?.run {
            ConsoleManager(userRepository, contactRepository, activityRepository).run()
        }
    }
}