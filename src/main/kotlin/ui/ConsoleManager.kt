package ui

import domain.Activity
import domain.Contact
import domain.Report
import domain.User
import exception.InvalidData
import repository.Repository
import validator.EmailValidator
import validator.PhoneNumberValidator
import java.io.InputStream
import java.sql.Time
import java.time.Duration
import java.util.*
import java.text.SimpleDateFormat

class ConsoleManager(
        private val userRepository: Repository<User>,
        private val contactRepository: Repository<Contact>,
        private val activityRepository: Repository<Activity>,
        private val scanner: Scanner = Scanner(System.`in`)) : Console() {

    enum class Option : DisplayOption {
        ADD_CONTACT, ADD_ACTIVITY, SHOW_REPORT, EXIT
    }

    override val getUserOptionFrom = fun(index: Int): DisplayOption =
            Option.values()[index - 1]

    override val optionMessage: HashMap<DisplayOption, String> = hashMapOf(
            Option.ADD_CONTACT to "1. Add a contact to your contact list.",
            Option.ADD_ACTIVITY to "2. Add an activity to your agenda.",
            Option.SHOW_REPORT to "3. Display your activity report.",
            Option.EXIT to "4. Exit")

    override val action: HashMap<DisplayOption, () -> Unit> = hashMapOf(
            Option.ADD_CONTACT to { readAndCreateContact().also { run() } },
            Option.ADD_ACTIVITY to { readAndCreateActivity().also { run() } },
            Option.SHOW_REPORT to { showReport().also { run() } },
            Option.EXIT to { })

    private fun readAndCreateContact() {
        val name = readInputData(withMessage = "Name: ")
        val address = readInputData(withMessage = "Address: ")
        val phoneNumber = readInputData(withMessage = "Phone Number: ")
        val email = readInputData(withMessage = "Email: ")
        try {
            EmailValidator().validate(email)
            PhoneNumberValidator().validate(phoneNumber)
            contactRepository.save(Contact(name, address, phoneNumber, email))
        } catch (error: InvalidData) {
            println(error.message)
        }
    }

    private fun readInputData(withMessage: String) =
            print(withMessage).let { scanner.nextLine() }

    fun readAndCreateActivity() {
        val title = readInputData(withMessage = "Title:")
        val description = readInputData(withMessage = "Description:")
        val location = readInputData(withMessage = "Location:")
        val startingHour = Time.valueOf(readInputData(
                withMessage = "Starting Hour (hh:mm:ss):"))
        val duration = Duration.ofHours(readInputData(
                withMessage = "Duration (ss):").toLong())
        contactRepository.all().forEach { println(it) }
        val contactsListAsString = readInputData(withMessage = "Contacts:")
        val contacts = contactsListAsString.split(",")
                .mapNotNull { contactRepository.findById(it.toInt()) }
        activityRepository.save(Activity(
                title, description, location, startingHour, duration, contacts))
    }

    fun showReport() {
        val dateFormatter = SimpleDateFormat("dd MM yyyy")
        val dateAsString = println("Date (DD MM YYYY): ")
                .let { Scanner(System.`in`).nextLine() }
        val date = dateFormatter.parse(dateAsString)
        print(Report(activityRepository, date))
    }
}