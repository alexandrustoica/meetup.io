package ui

import java.util.*

open class Console : Runnable {

    interface DisplayOption

    private enum class EmptyOption : DisplayOption

    protected open val optionMessage: HashMap<DisplayOption, String> = hashMapOf()

    protected open val action: HashMap<DisplayOption, () -> Unit> = hashMapOf()

    protected open val getUserOptionFrom =
            fun(index: Int): DisplayOption = EmptyOption.values()[index - 1]

    private fun executeOption(option: DisplayOption) = action[option]?.invoke()

    private fun readUserOption(): Int =
            print("Option: ").let { Scanner(System.`in`).nextInt() }

    private fun displayMenu() =
            optionMessage.values.sorted().forEach { println(it) }

    override fun run() {
        displayMenu().also {
            readUserOption().let { optionIndex ->
                executeOption(getUserOptionFrom(optionIndex))
            }
        }
    }
}